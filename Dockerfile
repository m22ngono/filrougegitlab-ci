FROM python:3.10.8-alpine

WORKDIR /usr/frontend/

COPY /vapormap-app/frontend .

ENV VAPORMAP_BACKEND=localhost
ENV VAPORMAP_BACKEND_PORT=5000

RUN apk add gettext

RUN envsubst '${VAPORMAP_BACKEND},${VAPORMAP_BACKEND_PORT}' < config.json.template > config.json

ENTRYPOINT python -m http.server
