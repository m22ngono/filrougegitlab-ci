# RAPPORT DU PROJET FIL ROUGE: PIPELINE CI/CD 

## NOM : NGONO NSA  
## PRENOM : Marie Tiffany  
## PROJET : Fil Rouge  
## CLASSE : MS-ICD  
## DATE/ANNEE : 10/01/2023  
## LIEN VERS DEPOT : https://gitlab.imt-atlantique.fr/m22ngono/filrougegitlab-ci.git  



# TRAVAIL REALISE  

### Dans la continuité du projet Vapormap, il est question ici de mettre en place un pipeline CI/CD qui exécutera un ensemble de tâches.  
### Mon pipeline est constitué de trois principaux stages: build, test et deploy. Ces stages représentent les 3 principales étapes qui seront exécutées. A chacune de ces étapes, j’ai déclaré des jobs qui représentent les tâches à effectuer.  

### La première partie a été l’intégration. L’intégration ici consiste à réaliser un ensemble de tests sur les images créées à partir des deux grandes parties de l’application à savoir le frontend et l’API.  

### Les tests ont pour but d’identifier l’ensemble des comportements problématiques de notre application dans le but de corriger les problèmes le plus rapidement possible, ce qui garantit un certain niveau de qualité du produit.  

### Dans mon cas, les tests effectués sont des linter d'images. Ces tests sont des programmes qui vont analyser mes images et m’indiquer s’il y a des problèmes ou non. Une fois les images scannées, les erreurs seront renvoyées en fonction des normes que respectent les outils de linter. Je vais par la suite rediriger ces resultats dans un fichier qui sera publié au niveau de Gitlab Pages.  

### Pour réaliser ces tests, une panoplie d’outils s’offrent à moi et mon choix s’est porté sur certains d’entre eux.  

## * API  

### L’API ayant été codé en python et utilise le Framework Flask. Il existe différents linter d’image python à savoir : Flake8, Pylint, PyFlakes. Mon choix s'est porté vers Flake8 car bien que Pylint soit un bon outil pour contrôler la qualité du code et les paramètres peuvent être modifiés pour eviter les surcharges, Flake8 est le meilleur outil pour évaluer la qualité globale du code, avec des recommandations spécifiques.  

## * Frontend  

### Le Frontend est écrit en HTML, CSS, JS, Bootstrap et comme linter nous avons: bootlint, JSLint, StyleLint.  
### StyleLInt est un outil qui permet d'éviter les erreurs CSS, il est flexible et teste plus d'une centaine de règles qu'il est possible d'activer en fonction des besoins. JSLint est un outil qui teste le code Javascript, est stable et fiable, est livré avec une configuration prête à l'emploi qui suit les meilleures pratiques de JS mais il n'est pas possible d'ajouter nos propres règles personnalisées ou désactiver la plupart des fonctionnalités. Ainsi mon choix s'est tourné vers bootlint car c'est un outil qui vérifie plusieurs erreurs HTML courantes dans les pages Web et qui utilisent Bootstrap. Cet outil est mieux adapté à mes besoins.  




### J'ai également effectué des tests sur les deux fichiers Dockerfile en utilisant l'outil Hadolint. Hadolint est l'outil le plus utilisé pour analyser les fichiers Dockerfile qui vise à les rendre conformes aux meilleures pratiques de construction d’images suggérées par Docker. J'ai publié les resultats d'analyse vers un fichier texte. Les tests sont non bloquants et le workflow est bien organisé.  

### La deuxième partie a été la partie Delivery. Dans cette partie j'ai réalisé des build des images du Frontend et de l'API. Quand on parle de build, c’est le fait de créer une version exécutable de l’application. Au niveau de mon pipeline CI/CD j’ai ecrit des jobs de build pour chacune de ces images en utilisant une image docker. Je vais d'abord me connecter a mon registre en utilisant des variables pour sécuriser mes informations, ensuite je vais faire un build des images et les stocker au niveau du registre de containers intégré a gitlab. Toutes les variables ont été bien utilisées dans le fichier docker-compose visant ainsi le bon fonctionnement de l'application à partir ces différentes images.  

### Afin de sécuriser le déploiement, j'ai également utilisé une image Mariadb que j'ai stocké dans la Registry. 



## Workflow  

![](workflow.png)  

## Registry Gitlab  
* Image du Frontend  
* Image du backend  
* Image de la base de données  

![](registry.png)  

## Pages  

![](pages.png) 




# RETOUR SUR EXPERIENCE  

### Tout au long de ce projet, j'ai rencontré différentes difficultées, notamment lors du choix des outils de linter. Le choix n'a pas été facile car les différents outils proposés sont tous bien, il était donc question de bien analyser mes besoins et faire les meilleurs choix. Donc je devais choisir un outil pouvant m'offrir les services recherchés c'est à dire detecter les problèmes rapidement afin que je puisse les corriger, performants et facile a utiliser.  

### Egalement au niveau des variables, qui n'étaient pas bien utilisées dans le docker-compose. Du coup j'ai dû bien les utiliser à ce niveau, reconstruire mes images, et que j'ai par la suite uilisé dans mon pipeline.  

### Et enfin j'ai eu du mal à rédiger ce rapport, on a tendance à penser que la partie technique est plus compliquée, mais en faite c'est la partie rédaction qui l'est. Mais cela reste une belle experience car un bon ingénieur est celui là qui sait allier non seulement une étude mais également la mise en place de la solution, quand on parle d'étude, on parle ici de toute la partie documentation.  





