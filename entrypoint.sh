#!/bin/sh


cd api
    
flask db upgrade || exit 1

cd ..

gunicorn --bind 0.0.0.0:5000 wsgi:app 

